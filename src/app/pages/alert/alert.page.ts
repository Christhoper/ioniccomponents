import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.page.html',
  styleUrls: ['./alert.page.scss'],
})
export class AlertPage implements OnInit {

  title: string;

  constructor(public alertCtrl: AlertController, public alertPromCtrl: AlertController) { }

  ngOnInit() {
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'colorDelete',
          handler: (blah) => {
            console.log('Cancel');
          }
        },
        {
          text: 'Ok',
          handler: (blah) => {
            console.log('Confirmar');
          }
        }
      ]
    });

    await alert.present();
  }

  async presentInput() {
    const input = await this.alertPromCtrl.create({
      header: 'Input',
      subHeader: 'Ingrese su nombre',
      inputs: [
        {
          name: 'txtNombre',
          type: 'text',
          placeholder: 'Nombre'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'colorDelete',
          handler: () => {
            console.log('Confirm');
          }
        }, {
          text: 'Ok',
          handler: ( data ) => {
            console.log('Confirm Ok');
            this.title= data.txtNombre;
          }
        }
      ]
    });

    await input.present();
  }




}
